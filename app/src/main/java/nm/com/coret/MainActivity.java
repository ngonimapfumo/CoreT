package nm.com.coret;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mDialButton, mWebButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDialButton = findViewById(R.id.uri_button);
        mWebButton=findViewById(R.id.web_button);
        mDialButton.setOnClickListener(this);
        mWebButton.setOnClickListener(this);}


    @Override
    public void onClick(View view) {
        int position = view.getId();
        switch (position){
            case R.id.uri_button:
                Uri number = Uri.parse("tel:+263777222031");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
                break;


            case R.id.web_button:
                Uri webpage = Uri.parse("http://www.youtube.com");
                Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(webIntent);
                break;
        }
    }
}
